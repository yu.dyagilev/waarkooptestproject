@smoke-tests
Feature: Search for the product

    Scenario Outline: Check successful searching existing products by valid name
        When The user executes GET request to retrieve product list items by name "<validProductName>"
        Then The user sees available product list items for product
        Examples:
            | validProductName   |
            | pasta              |
            | cola               |

    Scenario Outline: Check searching existing products by valid name with empty response body
        When The user executes GET request to retrieve product list items by name "<productNameWithEmptyDetails>"
        Then The user sees empty product details response body
        Examples:
            | productNameWithEmptyDetails   |
            | orange                        |
            | apple                         |

    Scenario Outline: Check error getting when user searches products by invalid name
        When The user executes GET request to retrieve product list items by name "<invalidProductName>"
        Then The user does not see product list items by name "<invalidProductName>"
        Examples:
            | invalidProductName |
            | test               |
            | null               |
            | 0                  |
            | %$%^&*             |

    Scenario Outline: Check error getting when user searches product with empty name
        When The user executes GET request to retrieve product list items by name "<emptyProductName>"
        Then The user is getting not authenticated error in response body
        Examples:
            | emptyProductName   |
            |                    |

