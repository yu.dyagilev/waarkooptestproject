package waarkoop.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "error",
    "message",
    "requested_item",
    "served_by"
})
public class ProductSearchErrorDetailsResponse {

    @JsonProperty("error")
    private Boolean error;
    @JsonProperty("message")
    private String message;
    @JsonProperty("requested_item")
    private String requestedItem;
    @JsonProperty("served_by")
    private String servedBy;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("error")
    public Boolean getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(Boolean error) {
        this.error = error;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("requested_item")
    public String getRequestedItem() {
        return requestedItem;
    }

    @JsonProperty("requested_item")
    public void setRequestedItem(String requestedItem) {
        this.requestedItem = requestedItem;
    }

    @JsonProperty("served_by")
    public String getServedBy() {
        return servedBy;
    }

    @JsonProperty("served_by")
    public void setServedBy(String servedBy) {
        this.servedBy = servedBy;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ProductSearchErrorDetailsResponse.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("error");
        sb.append('=');
        sb.append(((this.error == null)?"<null>":this.error));
        sb.append(',');
        sb.append("message");
        sb.append('=');
        sb.append(((this.message == null)?"<null>":this.message));
        sb.append(',');
        sb.append("requestedItem");
        sb.append('=');
        sb.append(((this.requestedItem == null)?"<null>":this.requestedItem));
        sb.append(',');
        sb.append("servedBy");
        sb.append('=');
        sb.append(((this.servedBy == null)?"<null>":this.servedBy));
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.requestedItem == null)? 0 :this.requestedItem.hashCode()));
        result = ((result* 31)+((this.error == null)? 0 :this.error.hashCode()));
        result = ((result* 31)+((this.message == null)? 0 :this.message.hashCode()));
        result = ((result* 31)+((this.servedBy == null)? 0 :this.servedBy.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof ProductSearchErrorDetailsResponse)) {
            return false;
        }
        ProductSearchErrorDetailsResponse rhs = ((ProductSearchErrorDetailsResponse) other);
        return (((((Objects.equals(this.requestedItem, rhs.requestedItem))
                   && (Objects.equals(this.error, rhs.error)))
                  && (Objects.equals(this.message, rhs.message)))
                 && (Objects.equals(this.servedBy, rhs.servedBy))));
    }
}