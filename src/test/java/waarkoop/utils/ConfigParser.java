package waarkoop.utils;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

import java.security.InvalidParameterException;
import java.text.MessageFormat;

public class ConfigParser {

    private static final EnvironmentVariables environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();

    public static String getPropertyByName(String propertyName) {
        return environmentVariables
            .optionalProperty(propertyName)
            .orElseThrow(() -> new InvalidParameterException(
                MessageFormat.format("Missing property value for name {0}!", propertyName)));
    }
}
