package waarkoop.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;
import org.apache.http.HttpStatus;
import waarkoop.api.ProductsAPIService;
import waarkoop.matchers.CustomURLMatcher;
import waarkoop.models.ProductSearchErrorDetailsResponse;
import waarkoop.models.ProductSearchErrorResponse;
import waarkoop.models.ProductSearchResponse;

import java.util.Arrays;

import static io.restassured.http.ContentType.JSON;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static waarkoop.matchers.PriceFormatMatcher.isPriceFormatted;

public class ProductSearchSteps {

    private Response apiProductSearchResponse;

    @Steps
    public ProductsAPIService productsAPIService;

    @When("The user executes GET request to retrieve product list items by name {string}")
    public void the_user_executes_get_request_to_retrieve_product_list_items_by_name(String validProductName) {
        apiProductSearchResponse = productsAPIService.searchProductByName(validProductName);
    }

    @Then("The user sees available product list items for product")
    public void the_user_sees_available_product_list_items_for_product() {
        restAssuredThat(response -> apiProductSearchResponse.then()
                                                            .contentType(JSON)
                                                            .statusCode(HttpStatus.SC_OK));

        ProductSearchResponse[] successProductSearchResponses = apiProductSearchResponse.as(ProductSearchResponse[].class);

        Arrays.stream(successProductSearchResponses).forEach(productItem -> {
            assertThat(formattedAssertionReason("provider"), productItem.getProvider(), notNullValue());
            assertThat(formattedAssertionReason("title"), productItem.getTitle(), notNullValue());
            assertThat(formattedAssertionReason("url"), productItem.getUrl(), anyOf(notNullValue(), CustomURLMatcher.isValidURL()));
            assertThat(formattedAssertionReason("brand"), productItem.getBrand(), anyOf(nullValue(), notNullValue()));
            assertThat(formattedAssertionReason("price"), productItem.getPrice(), allOf(notNullValue(), isPriceFormatted()));
            assertThat(formattedAssertionReason("unit"), productItem.getUnit(), notNullValue());
            assertThat(formattedAssertionReason("isPromo"), productItem.getIsPromo(), notNullValue());
            assertThat(formattedAssertionReason("promoDetails"), productItem.getPromoDetails(), anyOf(emptyString(), notNullValue()));
            assertThat(formattedAssertionReason("image"), productItem.getImage(), allOf(notNullValue(), CustomURLMatcher.isValidURL()));
        });
    }

    @Then("The user does not see product list items by name {string}")
    public void the_user_does_not_see_product_list_items_by_name(String productName) {
        restAssuredThat(response -> apiProductSearchResponse.then()
                                                            .contentType(JSON)
                                                            .statusCode(HttpStatus.SC_NOT_FOUND));

        ProductSearchErrorResponse errorProductSearchResponse = apiProductSearchResponse.as(ProductSearchErrorResponse.class);
        ProductSearchErrorDetailsResponse errorDetails = errorProductSearchResponse.getDetail();

        assertThat("Invalid error state!", errorDetails.getError(), is(true));
        assertThat("Invalid error message!", errorDetails.getMessage(), is("Not found"));
        assertThat("Invalid requested item!", errorDetails.getRequestedItem(), is(productName));
        assertThat("Invalid served by!", errorDetails.getServedBy(), is("https://waarkoop.com"));
    }

    @Then("The user sees empty product details response body")
    public void the_user_sees_empty_product_details_response_body() {
        restAssuredThat(response -> apiProductSearchResponse.then()
                                                            .contentType(JSON)
                                                            .statusCode(HttpStatus.SC_OK));

        assertThat("Response body was not empty!", apiProductSearchResponse.asString(), is("[]"));
    }

    @Then("The user is getting not authenticated error in response body")
    public void the_user_is_getting_not_authenticated_error_in_response_body() {
        restAssuredThat(response -> apiProductSearchResponse.then()
                                                            .contentType(ContentType.JSON)
                                                            .statusCode(HttpStatus.SC_UNAUTHORIZED));

        apiProductSearchResponse.then().body("detail", equalTo("Not authenticated"));
    }

    private String formattedAssertionReason(String productParameter) {
        return String.format("Product '%s' parameter was not found!", productParameter);
    }
}