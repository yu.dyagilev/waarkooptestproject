package waarkoop.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import waarkoop.utils.ConfigParser;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static io.restassured.http.ContentType.JSON;

public abstract class AbstractAPIService {

    abstract public String getApiPath();

    public RequestSpecification setUp() {
        SerenityRest.setDefaultConfig(
            RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
            (cls, charset) -> {
                ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
                mapper.configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
                return mapper;
            })));

        return SerenityRest.given()
                           .baseUri(getBaseUrl())
                           .contentType(JSON)
                           .accept(JSON).log().ifValidationFails();
    }

    private static String getBaseUrl() {
        return ConfigParser.getPropertyByName("products-api.baseUrl");
    }
}