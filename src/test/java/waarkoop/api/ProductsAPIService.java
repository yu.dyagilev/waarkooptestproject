package waarkoop.api;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;

import static java.lang.String.format;

public class ProductsAPIService extends AbstractAPIService {

    @Step
    public Response searchProductByName(String productName) {
        return setUp()
            .basePath(format(getApiPath(), productName))
            .get();
    }

    @Override
    public String getApiPath() {
        return "/search/demo/%s";
    }
}