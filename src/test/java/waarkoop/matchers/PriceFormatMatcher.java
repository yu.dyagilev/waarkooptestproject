package waarkoop.matchers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.text.DecimalFormat;

public class PriceFormatMatcher extends TypeSafeMatcher<Float> {

    @Override
    protected boolean matchesSafely(Float price) {
        DecimalFormat df = new DecimalFormat("#.00");
        float parsedPrice = Float.parseFloat(df.format(price));
        return parsedPrice == price;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("Price is not formatted correctly!");
    }

    public static Matcher<Float> isPriceFormatted() {
        return new PriceFormatMatcher();
    }
}