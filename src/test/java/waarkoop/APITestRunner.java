package waarkoop;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(monochrome = true,
    plugin = {"html:target/cucumber-reports", "json:target/cucumber.json", "pretty"},
    tags = "@smoke-tests",
    features = "src/test/resources/features"
)
public class APITestRunner {
}

